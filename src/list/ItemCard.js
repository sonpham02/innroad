import React from "react";
import {css} from "emotion";

const itemCard = css`
    background-color: #fff;
    border-radius: 3px;
    box-shadow: 0 1px 0 #ccc;
    border: 1px solid #ccc;
    padding: 10px;
    
    margin: 10px 0;
    
    &:hover {
        background-color: #e2e4e6;
    }
    
    .item-title {
        font-weight: 700;
        margin-bottom: 10px;
    }
`;

export class ItemCard extends React.Component {
    render () {
        const {item} = this.props;

        return (
            <div className={itemCard}>
                <div className="item-title">
                    {item.ItemTitle}
                </div>

                <div>
                    {item.Description}
                </div>
            </div>
        )
    }
}
