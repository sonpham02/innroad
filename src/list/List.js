import React from "react";
import {ItemCard} from "./ItemCard";
import {css} from "emotion";

const list = css`
    width: 100%;

    @media (min-width: 400px) {
        width: 47%;
        flex: 0 0 47%;
        
        &:not(:last-child) {
            margin-right: 20px;
        }
    }
    
    @media (min-width: 769px) {
        width: 272px;
        flex: 0 0 272px;
    }
    
    .title {
        font-size: 18px;
        font-weight: 800;
    }
`;

export class List extends React.Component {
    render () {
        const {title, items} = this.props;
        return (
            <div className={list}>
                <div className="title">
                    {title}
                </div>

                {items.map((item) => (
                    <ItemCard {...{item}} key={item.ItemId}/>
                ))}
            </div>
        )
    }
}
