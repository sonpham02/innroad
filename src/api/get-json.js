const {host} = require("./host");

export const getJson = (url) => {
    return fetch(
        `${host}${url}`
    )
        .then((resp) => resp.json())
        ;
};

