import {getJson} from "./get-json";

export const bucketsApi = {
    getList: () => getJson(`api/buckets`)
};
