import React, {Component} from 'react';
import logo from './logo.svg';
import {bucketsApi} from "./api/buckets-api";
import {List} from "./list/List";
import {css} from "emotion";

const list = css`
    @media (max-width: 400px) {
        display: block;
    }
    
    @media (max-width: 768px) {
        flex-wrap: wrap;
    }

    display: flex;
    overflow-x: auto;
    padding: 20px;
`;

const app = css`
    position: relative;
    margin-top: 3em;
    
    .app-header {
        position: fixed;
        width: 100%;
        top: 0;
        background: #fff;
        padding: 10px;
        text-align: center;
        border-bottom: 1px solid #ccc;
        
        .app-title {
            font-size: 1.5em;
            font-weight: 800;
        }
    }
    
    .loading {
        text-align: center;
        
        .app-logo {
            animation: app-logo-spin infinite 5s linear;
            height: 80px;
        }
        
        @keyframes app-logo-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }
    }
    
    
    
`;

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buckets: [],
            loading: false
        };

    }

    componentDidMount() {
        this.setState({loading: true});

        bucketsApi.getList().then((buckets) => this.setState({buckets, loading: false}));
    }

    render() {
        const {buckets , loading} = this.state;

        return (
            <div className={app}>
                <div className="app-header">
                    <div className="app-title">Inn Road</div>
                </div>

                { loading ? (
                    <div className="loading">
                        <img src={logo} className="app-logo" alt="logo" />
                    </div>
                ) : (
                    <div className={list}>
                        { buckets.map((bucket) => (
                            <List
                                key={bucket.BucketId}
                                title={bucket.Description}
                                items={bucket.Items}
                            />
                        ))}
                    </div>
                )}
            </div>
        );
    }
}

export default App;
